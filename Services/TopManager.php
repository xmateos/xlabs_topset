<?php

namespace XLabs\TopSetBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use XLabs\TopSetBundle\Entity\Top;
use XLabs\TopSetBundle\Entity\Entry;
use \DateTime;
use Doctrine\DBAL\Cache\QueryCacheProfile;

class TopManager
{
    private $em;
    private $config;

    public function __construct(EntityManagerInterface $em, $config)
    {
        $this->em = $em;
        $this->config = $config;
    }

    /*
     * Return all the entries (ids) for a top -> for frontend usage
     */
    public function getTopEntries($top_id)
    {
        $qb = $this->em->createQueryBuilder();
        $today = new DateTime();

        $query = $qb->select('e.object_id')
            //->addSelect('e_t')
            ->from(Entry::class, 'e')
            ->join('e.top', 'e_t', 'WITH', $qb->expr()->eq('e_t.id', $top_id))
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->isNull('e.startdate'),
                    $qb->expr()->andX(
                        $qb->expr()->lte('DATE(e.startdate)', $qb->expr()->literal($today->format('Y-m-d'))),
                        $qb->expr()->gte('DATE(e.enddate)', $qb->expr()->literal($today->format('Y-m-d')))
                    )
                )
            )
            ->orderBy('e.position', 'ASC')
            ->getQuery();

        //$cache = $this->em->getConfiguration()->getResultCacheImpl();
        //$hydrationCacheProfile = new QueryCacheProfile(Top::RESULT_CACHE_ITEM_TTL, Top::RESULT_CACHE_ITEM_PREFIX.$top_id.'_hydration', $cache);

        $results = $query
            ->useQueryCache(true)
            ->setResultCacheLifetime(Top::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(Top::RESULT_CACHE_ITEM_PREFIX.$top_id)
            //->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();

        if($results)
        {
            array_walk($results, function (&$value, $key) use (&$entries) {
                $entries[] = $value['object_id'];
            });
            return $entries;
        }
        return array();
    }
}