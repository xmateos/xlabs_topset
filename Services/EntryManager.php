<?php

namespace XLabs\TopSetBundle\Services;

use Symfony\Component\Routing\RouterInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use XLabs\TopSetBundle\Annotations\Config as XLabsTopSetConfig;

class EntryManager
{
    private $router;
    private $config;

    public function __construct(RouterInterface $router, $config)
    {
        $this->router = $router;
        $this->config = $config;
    }

    public function getAnnotation($FQCN)
    {
        // Get properties from annotation in Entity
        $reader = new AnnotationReader();
        $ref = new \ReflectionClass($FQCN);
        return $reader->getClassAnnotation($ref, XLabsTopSetConfig::$annotationName);
    }

    public function getEntriesJSON($FQCN)
    {
        $XLabsTopSetAnnotation = $this->getAnnotation($FQCN);

        if($XLabsTopSetAnnotation && $XLabsTopSetAnnotation->routeJSON)
        {
            $url = $this->router->generate($XLabsTopSetAnnotation->routeJSON, array(), RouterInterface::ABSOLUTE_URL);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_REFERER, 'https://www.gasm.com');
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

            $curl_response = curl_exec($ch);
            $error = false;
            if(curl_error($ch))
            {
                $error = true;
                dump(curl_errno($ch));
                dump(curl_error($ch));
            }
            curl_close($ch);
            return $error ? json_encode(array()) : $curl_response;
        } else {
            return null;
        }
        return json_encode(array());
    }

    /*public function getEntriesQB($FQCN)
    {
        $XLabsTopSetAnnotation = $this->getAnnotation($FQCN);
        if($XLabsTopSetAnnotation && property_exists($XLabsTopSetAnnotation, 'id') && property_exists($XLabsTopSetAnnotation, 'title') && property_exists($XLabsTopSetAnnotation, 'image'))
        {
            $qb = $this->em->createQueryBuilder();
            $fields = array();
            if($XLabsTopSetAnnotation->id)
            {
                $fields[] = 'e.'.$XLabsTopSetAnnotation->id.' AS id';
            }
            if($XLabsTopSetAnnotation->title)
            {
                $fields[] = 'e.'.$XLabsTopSetAnnotation->title.' AS title';
            }
            if($XLabsTopSetAnnotation->image)
            {
                $fields[] = 'e.'.$XLabsTopSetAnnotation->image.' AS image';
            }
            $qb->select(implode(',', $fields))
                ->from($FQCN, 'e');

            return $qb;
        }
        return false;
    }*/
}