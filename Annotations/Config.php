<?php

namespace XLabs\TopSetBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class Config extends Annotation
{
    public static $annotationName = 'XLabs\\TopSetBundle\\Annotations\\Config';

    /**
     * @var array<string>
     */
    public $routeJSON;
}