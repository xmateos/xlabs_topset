A redis driven result cache driver.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/topsetbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\TopSetBundle\XLabsTopSetBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_top_set:
    resource: .
    type: xlabs_topset_routing
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_top_set:
    # for firewall purposes; default is empty
    url_prefix: /members
```

## Usage ##
You can access the top manager tool by using the following routing id:
``` yml
xlabs_topset_manager
```

Check the manager main window; the plug icon will describe how to use it in the project.

Whenever you want an Entity to be reachable from the top manager, just include the following annotation in it:
```php
namespace YourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
...
use XLabs\TopSetBundle\Annotations as XLabsTopSet;

/**
 * @ORM\Entity
 * ...
 * @XLabsTopSet\Config(routeJSON="<your_routing_id>")
 */
class MyEntity
{
    ...
}
```
<your_routing_id> is the routing ID that will return all rows from that entity in JSON format, where each entry needs to be as follows:
```php
    $entries = array();
    ... get results ...
    ... loop begin ...
        $entries[$row['id']] = array(
            'id' => $row['id'],
            'title' => $row['title'],
            'image' => $row['image']
        );
    ... loop end ...
    return json_encode($entries);
```

The search in the Top Entries Manager form will be performed on the "title" attribute.