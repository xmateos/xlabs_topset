<?php

namespace XLabs\TopSetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use XLabs\TopSetBundle\Entity\Top;
use XLabs\TopSetBundle\Form\TopType;
use XLabs\TopSetBundle\Form\TopEntriesType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TopController extends Controller
{
    public function managerAction()
    {
        $em = $this->getDoctrine()->getManager();
        $top_sets = $em->getRepository('XLabsTopSetBundle:Top')->findAll();

        return $this->render('XLabsTopSetBundle:Top:manager.html.twig', array(
            'top_sets' => $top_sets
        ));
    }

    public function formAction($top_id)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $top = $top_id ? $em->getRepository('XLabsTopSetBundle:Top')->findOneBy(array(
            'id' => $top_id
        )) : new Top();
        $form = $this->createForm(TopType::class, $top, array(
            'entity_manager' => $em
        ));

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em->persist($top);
            $em->flush();

            return $this->render('XLabsTopSetBundle::fancybox_close.html.twig');
        }

        $response = $this->render('XLabsTopSetBundle:Top:form.html.twig', array(
            'top' => $top,
            'form' => $form->createView()
        ));
        return $response;
    }

    public function deleteAction($top_id)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $top = $em->getRepository('XLabsTopSetBundle:Top')->findOneBy(array(
            'id' => $top_id
        ));

        try {
            $em->remove($top);
            $em->flush();
        } catch(DBALException $e) {
            $this->get('session')->getFlashBag()->add('mm_top_error', $e->getMessage());
        }

        return $this->redirect($this->generateUrl('xlabs_topset_manager'));
    }

    public function entriesAction($top_id)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $top = $em->getRepository('XLabsTopSetBundle:Top')->findOneBy(array(
            'id' => $top_id
        ));

        // Original collection handler
        $original_entries = new ArrayCollection();
        foreach($top->getEntries() as $item)
        {
            $original_entries->add($item);
        }

        // Read annotation in Entity to get the JSON list
        $entry_manager = $this->get('xlabs_topset_entry_manager');
        $json_list = $entry_manager->getEntriesJSON($top->getEntityFQCN());

        if(is_null($json_list))
        {
            throw new NotFoundHttpException('You need to create annotation in "'.$top->getEntityFQCN().'" entity for the JSON list:  @XLabsTopSet\Config(routeJSON="<your_routing_id>")');
        }

        $form = $this->createForm(TopEntriesType::class, $top, array(
            'json_list' => $json_list
        ));

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            try {
                // Collection handler
                foreach($original_entries as $item)
                {
                    if(!$top->getEntries()->contains($item))
                    {
                        $em->remove($item);
                    }
                }

                $em->persist($top);
                $em->flush();
                return $this->render('MMCoreBundle::fancybox_close.html.twig');
            } catch(DBALException $e) {
                $this->get('session')->getFlashBag()->add('mm_top_error', $e->getMessage());
            }

            return $this->render('XLabsTopSetBundle::fancybox_close.html.twig');
        }

        $response = $this->render('XLabsTopSetBundle:Top:entries.html.twig', array(
            'top' => $top,
            'form' => $form->createView(),
            'json_list' => $json_list
        ));
        return $response;
    }

    public function usageAction($top_id)
    {
        $em = $this->getDoctrine()->getManager();
        $top = $em->getRepository('XLabsTopSetBundle:Top')->findOneBy(array(
            'id' => $top_id
        ));
        return $this->render('XLabsTopSetBundle:Top:usage.html.twig', array(
            'top' => $top
        ));
    }
}
