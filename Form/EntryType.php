<?php

namespace XLabs\TopSetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use XLabs\TopSetBundle\Form\CustomFormFields\DateRangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use XLabs\TopSetBundle\Entity\Entry;

class EntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object_id', TextType::class, array(
                'label' => '',
                'required' => true
            ))
            ->add('position', TextType::class, array(
                'label' => '',
                'required' => true
            ))
            ->add('dateRange', DateRangeType::class, array(
                'label' => '',
                'required' => false
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'xlabs_topset_bundle_entry_type';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Entry::class,
            'json_list' => json_encode(array())
        ));
        $resolver->setRequired('json_list');
    }
}
