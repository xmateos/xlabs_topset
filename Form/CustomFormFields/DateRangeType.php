<?php

namespace XLabs\TopSetBundle\Form\CustomFormFields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\Options;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\TopSetBundle\Form\CustomFormFields\DataTransformer\DateRangeTransformer;

class DateRangeType extends AbstractType
{
    private $config;
    private $em;

    public function __construct(EntityManagerInterface $em, $config)
    {
        $this->config = $config;
        $this->em = $em;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'class' => 'dateRange_selection'
            )
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new DateRangeTransformer($this->em, $options);
        $builder->addModelTransformer($transformer);
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'date_range';
    }
}