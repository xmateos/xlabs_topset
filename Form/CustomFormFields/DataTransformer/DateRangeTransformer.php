<?php

namespace XLabs\TopSetBundle\Form\CustomFormFields\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\ORM\EntityManagerInterface;

class DateRangeTransformer implements DataTransformerInterface
{
    private $em;
    private $options;

    public function __construct(EntityManagerInterface $em, $options)
    {
        $this->em = $em;
        $this->options = $options;
    }

    /*
     * Transforms an object to a string
     */
    public function transform($val)
    {
        return $val;
    }

    /*
     * Transforms a string to an object.
     */
    public function reverseTransform($val)
    {
        return $val;
    }
}