<?php

namespace XLabs\TopSetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use XLabs\TopSetBundle\Entity\Top;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TopEntriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entries', CollectionType::class, array(
                'entry_type' => EntryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options' => array(
                    'label' => false,
                    'json_list' => $options['json_list'],
                    'attr' => array(

                    )
                ),
                'prototype' => true
            ))
        ;
    }

    public function getEntries($options)
    {
        $em = $options['entity_manager'];
        $router = $options['router'];
        $top = $options['data'];

        $entries = array();
        $entries_ids = $top->getEntries()->map(function($item){
            return $item->getObjectId();
        })->toArray();
        if(count($entries_ids))
        {
            $entityRepository = $em->getRepository($top->getEntityFQCN());
            if(method_exists($entityRepository,'getXLTSentries'))
            {
                $entries = $entityRepository->getXLTSentries($entries_ids);
            } else {
                throw new NotFoundHttpException('You need to create method getXLTSentries in "'.$top->getEntityFQCN().'" repository that will return (id, title, image) for a given array of ids.');
            }
        }
        return $entries;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Top::class,
        ));
        $resolver->setRequired('json_list');
    }

    public function getBlockPrefix()
    {
        return 'xlabs_topset_bundle_top_type';
    }
}