<?php

namespace XLabs\TopSetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use XLabs\TopSetBundle\Entity\Top;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Doctrine\Common\Annotations\AnnotationReader;
use XLabs\TopSetBundle\Annotations\Config as XLabsTopSetConfig;

class TopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class, array(
                'required' => true,
                'attr' => array(
                    'style' => 'width: 300px;',
                    'maxlength' => '255'
                )
            ))
            ;

        $factory = $builder->getFormFactory();
        $em = $options['entity_manager'];
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use($factory, $em){
            $form = $event->getForm();
            $top = $event->getData();

            // Allow only to change entityType when creating it
            if(!$top || !$top->getId())
            {
                $entities = array(
                    '' => 'Select one ...'
                );
                $meta = $em->getMetadataFactory()->getAllMetadata();
                $reader = new AnnotationReader();
                foreach($meta as $m)
                {
                    $entity = $m->getName();
                    $ref = new \ReflectionClass($entity);
                    $XLabsTopSetAnnotation = $reader->getClassAnnotation($ref, XLabsTopSetConfig::$annotationName);
                    if($XLabsTopSetAnnotation && property_exists($XLabsTopSetAnnotation, 'routeJSON'))
                    {
                        $entities[$m->getName()] = $ref->getShortName().' ('.$m->getName().')';
                    }
                }

                $form->add('entity_fqcn', ChoiceType::class,array(
                    'required' => true,
                    'choices' => array_flip($entities)
                ));
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Top::class,
        ));
        $resolver->setRequired('entity_manager');
    }

    public function getBlockPrefix()
    {
        return 'xlabs_topset_bundle_top_type';
    }
}