<?php

namespace XLabs\TopSetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use XLabs\TopSetBundle\Entity\Entry;
use Doctrine\Common\Collections\ArrayCollection;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity
 * @ORM\Table(name="xltsb_topsets")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection")
 * })
 */
class Top
{
    const RESULT_CACHE_ITEM_PREFIX = 'mm_topset_';
    //const RESULT_CACHE_ITEM_TTL = 604800; // 1 week
    const RESULT_CACHE_ITEM_TTL = 86400; // 24h
    const RESULT_CACHE_COLLECTION_PREFIX = 'mm_topsets_';
    const RESULT_CACHE_COLLECTION_TTL = 3600;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @ORM\Column(name="entity_fqcn", type="string", length=255, nullable=false)
     */
    private $entity_fqcn;

    public function getEntityFQCN()
    {
        return $this->entity_fqcn;
    }

    public function setEntityFQCN($entity_fqcn)
    {
        $this->entity_fqcn = $entity_fqcn;
    }

    /**
     * @ORM\OneToMany(targetEntity="XLabs\TopSetBundle\Entity\Entry", mappedBy="top", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $entries;

    public function getEntries()
    {
        return $this->entries;
    }

    public function addEntry(Entry $entry)
    {
        $this->entries->add($entry);
        $entry->setTop($this);
    }

    public function removeEntry(Entry$entry)
    {
        $this->entries->removeElement($entry);
    }

    public function __construct()
    {
        $this->entries = new ArrayCollection();
    }

    public function getXLabsResultCacheKeyForItem()
    {
        return $this::RESULT_CACHE_ITEM_PREFIX.$this->getId();
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        return $this::RESULT_CACHE_COLLECTION_PREFIX;
    }
}