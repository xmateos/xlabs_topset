<?php

namespace XLabs\TopSetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;
use XLabs\TopSetBundle\Entity\Top;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity
 * @ORM\Table(name="xltsb_entries")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem")
 * })
 */
class Entry
{
    const RESULT_CACHE_ITEM_PREFIX = 'mm_topset_entry_';
    const RESULT_CACHE_ITEM_TTL = 604800; // 1 week

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="object_id", type="string", length=255, nullable=false)
     */
    private $object_id;

    public function getObjectId()
    {
        return $this->object_id;
    }

    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;
    }

    /**
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $position;

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @ORM\Column(name="startdate", type="datetime", nullable=true)
     */
    private $startdate;

    public function getStartdate()
    {
        return $this->startdate;
    }

    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }

    /**
     * @ORM\Column(name="enddate", type="datetime", nullable=true)
     */
    private $enddate;

    public function getEnddate()
    {
        return $this->enddate;
    }

    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    }

    /**
     * Property to handle date range in forms
     */
    private $dateRange;

    public function getDateRange()
    {
        if($this->getStartdate() && $this->getEnddate())
        {
            $this->dateRange = $this->getStartdate()->format('d-m-Y').' to '.$this->getEnddate()->format('d-m-Y');
        }
        return $this->dateRange;
    }

    public function setDateRange($dateRange)
    {
        if($dateRange)
        {
            $dates = explode(' to ', $dateRange);
            if(count($dates) == 2)
            {
                $startdate = $dates[0];
                $enddate = $dates[1];
                $this->setStartdate(DateTime::createFromFormat('d-m-Y', $startdate));
                $this->setEnddate(DateTime::createFromFormat('d-m-Y', $enddate));
            }
        } else {
            $this->setStartdate(null);
            $this->setEnddate(null);
        }
        $this->dateRange = $dateRange;
    }

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\TopSetBundle\Entity\Top", inversedBy="entries")
     */
    protected $top;

    public function getTop()
    {
        return $this->top;
    }

    public function setTop($top)
    {
        $this->top = $top;
    }

    public function __construct()
    {
        //$this->startdate = new DateTime();
        //$this->enddate = new DateTime();
    }

    public function getXLabsResultCacheKeyForItem()
    {
        return array(
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId(),
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId().'_hydration',
            Top::RESULT_CACHE_ITEM_PREFIX.$this->getTop()->getId(),
            Top::RESULT_CACHE_ITEM_PREFIX.$this->getTop()->getId().'_hydration'
        );
    }
}