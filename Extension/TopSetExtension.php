<?php

namespace XLabs\TopSetBundle\Extension;

use XLabs\TopSetBundle\Services\TopManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use XLabs\TopSetBundle\Entity\Top;

class TopSetExtension extends AbstractExtension
{
    private $top_manager;
    private $config;

    public function __construct(TopManager $top_manager, $config)
    {
        $this->top_manager = $top_manager;
        $this->config = $config;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('getTopEntries', array($this, 'getTopEntries')),
            new TwigFunction('getTopFQCN', array($this, 'getTopFQCN')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    /*
     * Return all the entries (ids) for a top -> for frontend usage
     */
    public function getTopEntries($top_id)
    {
        return $this->top_manager->getTopEntries($top_id);
    }

    public function getTopFQCN($top)
    {
        $fqcn = '';
        if($top instanceof Top)
        {
            $ref = new \ReflectionClass($top->getEntityFQCN());
            $fqcn = $ref->getShortName();
        } else {
            if(is_array($top) && isset($top['entity_fqcn']))
            {
                $ref = new \ReflectionClass($top['entity_fqcn']);
                $fqcn = $ref->getShortName();
            }
        }
        return $fqcn;
    }
}