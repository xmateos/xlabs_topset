function getEntryById(filter)
{
    return filter in json_list ? json_list[filter] : false;
}

function searchByIdOrTitle(filter)
{
    var key, keys = [];
    if(filter != '')
    {
        filter = new RegExp(filter.toLowerCase());
        for (key in json_list) {
            if (json_list.hasOwnProperty(key))
            {
                if((json_list[key].title && filter.test(json_list[key].title.toLowerCase()) || (json_list[key].id) && filter.test(json_list[key].id)))
                {
                    keys.push(key);
                }
            }
        }
    }
    return keys;
}

function addEntryItem(collection_item, entry_data)
{
    let entry_item = $('#sortable_entries .entry_item.sample').clone();
    entry_item.removeClass('sample');

    // Position
    entry_item.find('.entry_item_position').text(entry_data.position);

    // Image
    if(entry_data.image)
    {
        entry_item.find('.entry_item_img').show().find('img').attr('src', entry_data.image);
    } else {
        entry_item.find('.entry_item_no_img').show().find('img').attr('src', '/bundles/xlabstopset/images/no_image.png');
    }
    entry_item.find('.entry_item_image').text(entry_data.image ? entry_data.image : '');

    // Title
    entry_item.find('.entry_item_title').attr('title', entry_data.title).text(entry_data.title);

    // ID
    entry_item.find('.entry_item_id').text(entry_data.id);

    // Scheduling
    if(entry_data.startdate && entry_data.enddate)
    {
        let now = new Date();
        let startdate_str = getFriendlyDateStr(entry_data.startdate);
        let enddate_str = getFriendlyDateStr(entry_data.enddate);
        now = now.setHours(0,0,0,0);
        let startdate = entry_data.startdate.setHours(0,0,0,0);
        let enddate = entry_data.enddate.setHours(0,0,0,0);

        switch(true)
        {
            case now > enddate:
                entry_item.find('.entry_item_date_range label.inactive').show().append('From ' + startdate_str + ' to ' + enddate_str);
                entry_item.find('.entry_item_date_status').show().find('.entry_item_date_status_inactive').show();
                //entry_item.addClass('inactive');
                //collection_item.addClass('inactive');
                break;
            case startdate > now :
                entry_item.find('.entry_item_date_range label.active').show().append('From ' + startdate_str + ' to ' + enddate_str);
                entry_item.find('.entry_item_date_status').show().find('.entry_item_date_status_scheduled').show();
                //entry_item.addClass('scheduled');
                //collection_item.addClass('scheduled');
                break;
            default:
                entry_item.find('.entry_item_date_range label.active').show().append('From ' + startdate_str + ' to ' + enddate_str);
                entry_item.find('.entry_item_date_status').show().find('.entry_item_date_status_active').show();
                //entry_item.addClass('active');
                //collection_item.addClass('active');
                break;
        }
    } else {
        entry_item.find('.entry_item_date_range label.active').show().append('entry never expires');
        entry_item.find('.entry_item_date_status').show().find('.entry_item_date_status_active').show();
        //entry_item.addClass('active');
        //collection_item.addClass('active');
    }

    collection_item.find('fieldset:first').prepend(entry_item);
}

function getSearchResultItem(search_result_data)
{
    let search_result_item = $('#sortable_entries .search_result_item.sample').clone();
    search_result_item.removeClass('sample');

    // Image
    if(search_result_data.image)
    {
        search_result_item.find('.search_result_item_img').show().find('img').attr('src', search_result_data.image);
    } else {
        search_result_item.find('.search_result_item_no_img').show().find('img').attr('src', '/bundles/xlabstopset/images/no_image.png');
    }
    search_result_item.find('.search_result_item_image').text(search_result_data.image ? search_result_data.image : '');

    // Title
    search_result_item.find('.search_result_item_title').attr('title', search_result_data.title).text(search_result_data.title);

    // ID
    search_result_item.find('.search_result_item_id').text(search_result_data.id);

    // wrap inside dummy <p> to include the search_result_item container aswell
    return search_result_item.wrap('<p/>').parent().html();
}

function getFriendlyDateStr(dateObj)
{
    return (('0' + dateObj.getDate()).slice(-2)) + '-' + (('0' + (dateObj.getMonth() + 1)).slice(-2)) + '-' + dateObj.getFullYear();
}

// Event listener for when date range selected
function onDateRangeSelected_callback(data)
{
    let field = $(data.obj);
    let value = data.value.value;
    let collection_item = field.closest('.collection-item');
    let entry_item = collection_item.find('.entry_item');
    let entry_item_date_range_span = entry_item.find('.entry_item_date_range');
    let entry_item_status_span = entry_item.find('.entry_item_date_status');
    if(value)
    {
        let now = new Date();
        let startdate = data.value.date1;
        let enddate = data.value.date2;

        if(startdate && enddate)
        {
            let startdate_str = getFriendlyDateStr(startdate);
            let enddate_str = getFriendlyDateStr(enddate);
            now = now.setHours(0,0,0,0);
            startdate = startdate.setHours(0,0,0,0);
            enddate = enddate.setHours(0,0,0,0);

            switch(true)
            {
                case now > enddate:
                    entry_item_date_range_span.find('label.active').hide();
                    entry_item_date_range_span.find('label.inactive').show().text('From ' + startdate_str + ' to ' + enddate_str);
                    entry_item_status_span.show().find('.entry_item_date_status_active').hide();
                    entry_item_status_span.find('.entry_item_date_status_scheduled').hide();
                    entry_item_status_span.show().find('.entry_item_date_status_inactive').show();
                    entry_item.removeClass('active').removeClass('scheduled').addClass('inactive');
                    collection_item.removeClass('active').removeClass('scheduled').addClass('inactive');
                    break;
                case startdate > now:
                    entry_item_date_range_span.find('label.inactive').hide();
                    entry_item_date_range_span.find('label.active').show().text('From ' + startdate_str + ' to ' + enddate_str);
                    entry_item_status_span.find('.entry_item_date_status_inactive').hide();
                    entry_item_status_span.find('.entry_item_date_status_active').hide();
                    entry_item_status_span.show().find('.entry_item_date_status_scheduled').show();
                    entry_item.removeClass('active').removeClass('inactive').addClass('scheduled');
                    collection_item.removeClass('active').removeClass('inactive').addClass('scheduled');
                    break;
                default:
                    entry_item_date_range_span.find('label.inactive').hide();
                    entry_item_date_range_span.find('label.active').show().text('From ' + startdate_str + ' to ' + enddate_str);
                    entry_item_status_span.find('.entry_item_date_status_inactive').hide();
                    entry_item_status_span.find('.entry_item_date_status_scheduled').hide();
                    entry_item_status_span.show().find('.entry_item_date_status_active').show();
                    entry_item.removeClass('inactive').removeClass('scheduled').addClass('active');
                    collection_item.removeClass('inactive').removeClass('scheduled').addClass('active');
                    break;
            }
        } else {
            entry_item_date_range_span.find('label.inactive').hide();
            entry_item_date_range_span.find('label.active').show().text('entry never expires');
            entry_item_status_span.find('.entry_item_date_status_inactive').hide();
            entry_item_status_span.find('.entry_item_date_status_scheduled').hide();
            entry_item_status_span.show().find('.entry_item_date_status_active').show();
            entry_item.removeClass('inactive').removeClass('scheduled').addClass('active');
            collection_item.removeClass('inactive').removeClass('scheduled').addClass('active');
        }
    } else {
        entry_item_date_range_span.find('label.inactive').hide();
        entry_item_date_range_span.find('label.active').show().text('entry never expires');
        entry_item_status_span.find('.entry_item_date_status_inactive').hide();
        entry_item_status_span.find('.entry_item_date_status_scheduled').hide();
        entry_item_status_span.show().find('.entry_item_date_status_active').show();
        entry_item.removeClass('inactive').removeClass('scheduled').addClass('active');
        collection_item.removeClass('inactive').removeClass('scheduled').addClass('active');
    }
}