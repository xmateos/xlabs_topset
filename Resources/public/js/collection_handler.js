var COLLECTION_HANDLER = function(collection_holder_selector, prototype_name){
    var _this = this;
    this.collection_holder = $(collection_holder_selector);
    //this.index_incr = form_controls_amount; // total of inputs/textarea found in the subform
    this.prototype_name = typeof prototype_name === 'undefined' ? '__name__' : prototype_name; // defined in formType class; if not set, '__name__' is default

    this.add_button_text = 'Add';
    this.add_button = $('<a href="javascript:;" class="add-collection-item" style="color: #2c3338; font-size: 12px;"><i class="fa fa-plus-square" aria-hidden="true" style="margin-right: 8px;"></i><span>' + this.add_button_text + '</span></a>');
    this.add_button_container = $('<div />').addClass('add-collection-item-c').css({
        'width': '100%',
        'text-align': 'center',
        'position': 'relative',
        'margin': '12px 0'
    }).append(this.add_button);
    this.callbacks = {
        'onAdd': function(){},
        'onRemove': function(){}
    };

    this.addRemoveSubForm = function(elem) {
        var remove_button = $('<a href="javascript:;" class="remove-collection-item" style="color: #E2352E; font-size: 26px;"><i class="fa fa-times" aria-hidden="true"></i></a>');
        var remove_button_container = $('<div />').css({
            'position': 'absolute',
            'width': 'auto',
            'top': '2px',
            'right': '2px'
        }).append(remove_button);
        elem.append(remove_button_container);
        remove_button.on('click', function(e) {
            e.preventDefault();
            elem.remove();
            _this.callbacks['onRemove'](elem);
        });
    };

    this.addSubForm = function() {
        // Get the data-prototype explained earlier
        var prototype = this.collection_holder.data('prototype');

        // get the new index
        var index = this.collection_holder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var prototype_replace = new RegExp(this.prototype_name, 'g');
        var newForm = prototype.replace(prototype_replace, index);
        //var newForm = prototype;

        // increase the index with one for the next item
        this.collection_holder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var newFormBlock = $('<div />').append(newForm);

        // also add a remove button, just for this example
        this.addRemoveSubForm(newFormBlock.find('div.collection-item'));

        this.add_button_container.before(newFormBlock);

        this.callbacks['onAdd'](newFormBlock);

        return newFormBlock;
    };

    // in document.ready
    this.collection_holder.append(this.add_button_container);
    //this.collection_holder.data('index', (this.collection_holder.find(':input').length) / this.index_incr);
    this.collection_holder.data('index', (this.collection_holder.find(':input').length + this.collection_holder.find('textarea').length));

    this.add_button.on('click', function(e) {
        e.preventDefault();
        return _this.addSubForm();
    });

    this.collection_holder.find('div.collection-item').each(function() {
        _this.addRemoveSubForm($(this));
        _this.callbacks['onRemove']();
    });

    $('form').on('click', '.remove-collection-item', function(e) {
        e.preventDefault();
        $(this).closest('.collection-item').remove();
        return false;
    });

    return {
        init : function() {
            _this = this;
            return this;
        },
        triggerAdd : function() {
            return _this.addSubForm();
        },
        setCallback : function(method, func) {
            _this.callbacks[method] = func;
            return this;
        },
        setAddButtonText : function(text) {
            _this.add_button_text = text;
            _this.add_button_container.find('.add-collection-item > span').text(text);
            return this;
        }
    };
};