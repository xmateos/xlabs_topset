<?php

namespace XLabs\TopSetBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class TopSetRoutingLoader extends Loader
{
    private $config;
    private $loaded = false;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "xlabs_topset_routing" loader twice');
        }

        $routes = new RouteCollection();
        $aRoutes = array();

        $path_prefix = $this->config['url_prefix'];

        // Route for manager
        $path = $path_prefix.$this->config['manager_url'];
        $defaults = array(
            '_controller' => 'XLabsTopSetBundle:Top:manager',
        );
        /*$requirements = array(
            'parameter' => '\d+',
        );*/
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_topset_manager';
        $aRoutes[$routeName] = $route;

        // Route to create/edit top
        $path = $path_prefix.$this->config['top_form_url'];
        $defaults = array(
            '_controller' => 'XLabsTopSetBundle:Top:form',
            'top_id' => false
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_topset_form';
        $aRoutes[$routeName] = $route;

        // Route to edit top entries
        $path = $path_prefix.$this->config['top_entries_form_url'];
        $defaults = array(
            '_controller' => 'XLabsTopSetBundle:Top:entries',
            'top_id' => false
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_topset_form_entries';
        $aRoutes[$routeName] = $route;

        // Route to delete top
        $path = $path_prefix.$this->config['top_delete_url'];
        $defaults = array(
            '_controller' => 'XLabsTopSetBundle:Top:delete'
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_topset_delete';
        $aRoutes[$routeName] = $route;

        // Route to top usage
        $path = $path_prefix.$this->config['top_usage_url'];
        $defaults = array(
            '_controller' => 'XLabsTopSetBundle:Top:usage'
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_topset_usage';
        $aRoutes[$routeName] = $route;

        foreach($aRoutes as $routeName => $route)
        {
            $routes->add($routeName, $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'xlabs_topset_routing' === $type;
    }
}